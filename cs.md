<link href="style.css">

# Markdown Cheat Sheet

Det här dokumentet är skrivet i ett format som kallas *Markdown*. Markdown är ett enkelt sätt att formattera textdokument.

## Rubriker

För att göra en rubrik skriver man en hash `#` i början av raden, som i början av rubriken i detta avsnitt.

Ju fler `#` i början av raden, desto mindre rubrik.

Skriv en `#` i början av raden där det står *Rubrik 1*, `##` på raden
*Rubrik 3* och `###` på raden *Rubrik 3*.

# Rubrik 1

## Rubrik 2

### Rubrik 3

## Betoning

För att få viss ord att sticka ut kan man betona det genom att skriva en
asterisk `*` framför och efter ordet.

Betona ordet **emphasis** i denna mening genom att omge det med `*`.

Betona orden strong emphasis i denna mening genom att omge dem båda med `**`.

## Listor

Det finns två typer av listor:

* Punktlistor
* Numrerade listor

Punktlistor börjar med en asterisk på varje rad. Man kan göra en fruktsallad av
ingredienserna nedan. Gör om dem till en punktlista.

* Banan
* Vindruvor
* Kiwi
* Passionsfrukt
* Mango

Numrerade listor börjar med nummer och punkt. Listan nedan är inte komplett.
Lägg till punkterna 3 - 5 så att det blir rätt.

1. Skala bananen och skär den i skivor.
1. Skölj vindruvorna och dela dem.
1. Skala kiwi och skär i tärningar.
1. Gröp ur passionsfrukten.
1. Skala mangon och skär i stavar.
1. Blanda alla frukter i en skål.

## Kod

Genom att börja en rad med fyra blanksteg eller en tab gör man att texten blir
ett kodblock.

Skriv 4 blanksteg i början av nedanstående rader för att göra ett kodblock.

    x = 3
    y = 5
    if x > y:
        print("x is bigger than y")
    else:
        print("y is bigger than x")

Kodavsnitt kan även göras genom att ange text inom ` tecken.

Denna rad innehåller lite `kod som renderas annorlunda`. Gör ett kodavsnitt av
resten av raden: `x = foo() + bar`

## Länkar {.black}

Du kan göra en länk till en annan sida genom att ange texten som ska vara en
länk inom hakparanteser, [ och ]. Efter texten inom hakparanteser ska det finnas
en address till sidan du ska länka till inom vanliga paranteser ( och ).

`En länk till [Markdown Syntax](http://daringfireball.net/projects/markdown/syntax)`

blir

En länk till [Mark Syntax](http://daringfireball.net/projects/markdown/syntax)

Gör ordet [Wikipedia](https://www.wikipedia.org/) i denna mening till en länk till Wikipedias hemsida.

## Bilder

En länk till en bild gör du så här:

`![Patwic](http://www.oocities.org/sunsetstrip/amphitheatre/8707/rats1.JPG)`

![Patwic](data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxMTEhQUEhQWFRUWGRkaGBgYGBoYGBocHxsaHRYbHR8fHCggIBwlHB8XITEiJSkrLi4uHR8zODMsNygtLisBCgoKDA0NFA4NDisZFBksLDc3NysrKysrKys3LCsrKysrKysrKysrKyssLCwrKysrKysrKysrLCsrLCsrKysrK//AABEIAPkAywMBIgACEQEDEQH/xAAcAAABBQEBAQAAAAAAAAAAAAAAAwQFBgcCAQj/xABMEAACAQMCAwUEBgcECAMJAAABAgMABBESIQUxQQYTIlFhMnGBkQcUI0JSoTNicoKxwdGSorLwFSQ1Q1NzdLM0Y4MIFiWTwsPS4eP/xAAUAQEAAAAAAAAAAAAAAAAAAAAA/8QAFBEBAAAAAAAAAAAAAAAAAAAAAP/aAAwDAQACEQMRAD8A3GiiigKKKKAoopnc3uMKo1Mc4HLlzPoo2yfdjJIBBy8gHM/1qj/SV2xmsIRJH3KL07ws0kjfgRAMAY5uzbD7pOM1Dt329voxJDZqWkSRVlnWPUI2bSEgXJZTKTzxnGcDJBaoax+jzi3FLxZOK6o4wFLMxTOjmI40U4UnrkDG5OTsQ3nhd8s8MUyZ0yorrnY4YAjPrvTqkLG0SGNIolCRooVVHIADAFL0BRRRQcSSqvtED3kCiKVWGVYMPMEGqN2U4veTzXaXVv3aRSusU2NPegOQPCefhwdYwp8qnbbMLSuCumQh21fdIVVznPs4Ucx570E/XjMAMkgD1qDv+NmJC76QMquwJOWZUUDxcyzAUyu7pg+ZWzG33jgd23Lf/wAtvP7p35E4Cau+N28Yy8ijp1P8BTV+00J3jzIOunb8jj88VDcVgQowlOldt+RBz4SPXOMVXLzh7QKJEWS4wcaYtCNtzyWcddjgZ35eQaZYcQjmGY2zjmOTL7xzH8+lOqxXg3ae6muD/q6WpXVhHkZbjG26KyBWXlsduWTV8s+1Eit9uFZcjBRSGAwNzliGOcnbGxHluFuopK2uEkUOjBlPIjlStAUUUUBRRRQFFFFAUUUze5YnARseYx8fd/nryD2eU6sDcY5DmT7ycAD8/hulYoMF8EFjuWIJIGdIGNtI6AeZPMknqS2JB3AJIztqGByXGRt/n0ql/Sv2rfh9p4ZP9YlysQVQMficg5zpHwzigpX0vceaa8tOHWBCvHMrEp4QJ2Yd3y6qSWJxzPpW6oCAMnJxueWfWsa+g3sIRp4ndZMjhjArcwG2MrZ3LNvj0JO+RjZ6AooooEbrlg9f6Go9rdOqr7yB/OjtSlwbaQ2ePrCYeMMMqxUglDuNnXUnMY1ZyMZrGbmfjfG5O5WBrKFT9ozB0C+YyQCx/VAz5kA0G0Y5VQLe2W94abu9cyd5byP3fswReFvZQe0VxnVIWIIyMUr2E7P3vDLg2c031i0kjZ4n3BjkQrqTBJ0hlJOMkHTkYOqnHbWcMIOGQAB7ohXCjAitl/TNsMDKgoBtzPlQJ8S4bc3cVrGG7lFhSZpQQWM6qO4XT+FXxIc89KjzqW4FxP6zDqKhZFJjmj56JV2kT3dR5qVPWpgjy5UnpA5DnQMFslBXAOEOVUnKqRyKg7DHQDYUwubtBmWNhq32OQsoBOV359QGGSDy6gzLCo7ho0iS3POIl4/WJzkj9x8j3FaCB+kDhIuLNwql8aJkUjDEDDFccwxTUvvNL2scXdIYQoiKgoFAC6SMjAHpU04qNBjXwKUGCfCCNskk7e8mgkezXGFg+ykAWMnZsAaTyw36vLfp7uV0rMbmMllQEKWJGo76QFLNt1bAOB1NL23H5YFCIdSKfDrOSB5Ej7p/LbGw2DR6KZ8K4ik8YkT3EHmrDmp9R/Q08oCiiigKKK8Y0De8mCjc4/z/AJ/KkH4hGpI32DHYHHhKqwzyzlgOfn5HHk57wHutJbBwTyz0B25Fh8QPWkLfhLBhrYMoMZzjxPoXKhugxLqkyMkk+XMJOIkgEgqSAdJxkehwSMj0JFZ32/8Ao6k4lewStKq28agOpyWI1AsoxjGeWc7Vok8oRSzHAUEk+grHLb6ZZ5bt0gsxLApOMPpfGcaiT4d+g299Bc+O9rFsL6ztmwYrnEYUADu2zhCoA9jJRSDyBB6YN1r5wvnur7iguriNoUiIaMagcaTmMAg4LZwTjooHlVwbjNwc5nl3/Xb+tBofa3iZigbu5USXbAOCxGd8Dzx1x8uYz+y4tIsySPJI2lgT4iSQDuME43G1R5OTk7k8zRQXO97eMdoYgP1nOfyH9ad2/DzdW6PPJKWfJ8LaVAz4QFxp5Y6b1Qq0rs7KGtYsdF0n3jY0CPCOBR25LIWZiMZbGw6gYAo4hMBNEqohnkDqjtsFQaWk35nkp0jmQOWMiVNRnFWgK6ZcN1CjJfI5FQviBHmOVB61q33pGJ9AFHwxvj3k01iZluoYgxZWSRpA2+FUAKQdjnUR57ZpzwhZRDiYktqOjUQXCfdDkbF+e49Nzzry6skdlY5DLnSykqRnmMjmD5Hag7cVEcTkTKlXxMhJj0DW2+xUqOakbEHA9RjNPW4fH1XPvrpYwowoA91BHKnetpm8EmkN3CnCEfeOobvg80BwM76udEkK406VC/hCgD5UrxC11gYJV1OpHHNGHIj+Y6jIrmKfvYklwFZtSuo5CRDh8ehO499BFPOhiRZDmRNSONy/hc90225ymjDenpSMsR7tS4Icltj7WjbRr/W9r1xpzvmpKUVXuN2pmCNGVkCMwMZkKIzA48RUE6kYHwkEZzncDASPZ7jDWsgdgRG2kONjlTnSwweaEHPocdK1FGBAIOQdwRyI6ViUN53kLE6QVaQbHK6lJDqGwMqSMg4G5I5ttf8AsHxI4e0kOWizoPmoOGX904+DLQW+iiigKQn3IXz5+7r/ACHxNLE1HR3GlmD5Y9CBz64A896BbU2WESrsdydgT15b+W9d2cxdSSMYJG3I+opExP3b42ZyTjyzjb5UR3RUAd04x+EZHz2oKV9OHHDbcNZVOGnYRg+h3b+6G3rNuxXDBDbKxHjlAdj6H2B8F395Nax287NR8RjjjuO8jQPlShGrOlic5BwNOrbHUVK8P7JWsSgBNekAZc55bUGY17WgdtLq0tLV5JEQbHT4RnPLI+Yx5kgdaynsxfyzxGWVQup2KAfgzt8twD1AHvIT1lZSStpjUsfTp76sVl2MkODK4T0G5/pUZ2b4jJDKAg1ayAVHM+RHuyf87jS6CuxdkbcDfW3xx/Cm81ilnIrqW7iQhJQWYhCdo5BvgbnS3mNOfZFWg0x4zErQSq/slGz7sGg5fhyfe1N7yD/Kuo7dV9lQKS4JIzWtuz+00aE+fsjf+dLzKSCA2k+eAcfPagGphLxCMawG1Mgyyp42HlkLkjPr5Hyqk8Xv7rh1y0t+zXdhLhRIFA+r5P34lAVgcgasE7bbnSzbtH2dEaifhEmg3YMZgjIEE4KOSyfdjcIrEMMAY6EnIWW845I9rHPbxgCV4lj73OSsroiPpU8sNrxqB0jfBOA743dtDAzrgsNIBblkkLk49+ageE9o472S3hVHglgZpJ7d1IKBIyirnGCuuRCDsfDyFWTidoJonjbYMMZ8j0PwOKDi7s5Y2KiXVjG7KAc435bY+FJQxCOMRrk+J3JPNmY5J8q4h4tqQiba4iGHQDJkA2V0HNg3pyNIzxzqneN+kO/cbYCfhLc+8I+A5Y60HUtVTj3AXZZDBNIhc6jEH0RsSRrOoIXXIzkKcE+WSathdXRZIzlH5Z5qR7St5MDTOWggbLhwgi7pSCqltI0gYUkkLtzxn2jueZrqxvWgnSVckq2cefmP3gSvvIPSntwKjp4xpZyOpVAfMe2/uGwHqT+Gg2iCUOqspyrAEHzBGQflXdVf6PuI97bshO8TbfsN4k+WSn7lWigTmYAbnH+c/wAqYXnEFt4e8cMckAKg1OzyMAiKPMswG+B1JABNOruIOQDyG/5/0yPjUL23t+9te7MsMKu6hpZlDKmMspALKC+oKBuMZyNxQSPB+LLcBsLJG6HDxSrokTPLIyQVO+GUlTg4OxxIVU+yN1NLLqkuYp1W2gIKKgbW5cyFtPIeBcLy333GTMwxtKC+tlJPhwTgDptnH/7zz5UHXGP92OpY/wCB/wCoHxp/TOG1YtrkYMQMADkOvzzj5Dl1eUEH2q7LW9/GI7hSQOWCQRyP8qYwdiLdcDLFRgBdgMDkNulTfGeKRWsMk876I4xlj+QAHUkkADqSKiOEXd5dxrN4LSKQao0Kd7OVPsM5LBEJG+jS2MjxZyKCWsuGxQ/o0C+vU/GjiV2IYpJTuEUtjzwM4qmdtO0N7wru7h2S6tGcJICgjmQkEgqynQw2PNRvgdci28StxcW7oDtKhwfeMqf4UDe0s5GjVpJW71gGbBIRcjOkKCBgcs8zjnRccNaRSkshZDjKgFdQ8ick4PvrnhvFlMIaU6WXwyA81cbMMc9+Y8810k08viVREn3e8BMjfu5Gge/J8wKB+x+AHIUmaZcJ4h3ocHwvGxV1PNT094I3Bp4aBG4iV1ZXUMrAhlYAgg8wQdiKrPZ3sdFZzSPFI5iYHuoWJKQliDKV331aU6ZAB3OatDVGveu7tHAoYp+kdjiOPPIHG7N+qPiRQKm2TWZNC94VCl8DUVByFJ54BztQ1INYk+3LI5/VPdr8Au+PeTUfdQaZreOEnXJIMg+LwDeQnO/L1oJCRRkHAyOR6ikZKd3mNbaeWdqjbu7RPaYA9B94+4Dc/CgbWS6bkxD2LlWOPKVBqDD3rkHzwKa62kBMeNHIyN7GfJcbufdt605tYtd1D3ylQVm7uM+0fszqZx91SNgvM5OcVzcSE8+gwANgB0AHICgYRREsys2r7N3BwFwUwSMeRBPWmDLkOp5KrOvp4l1D3Etn0J9aeysytqXG6OhznYNpORjr4eXrUbe3SRro1gNIRuxALkeyoH4QTyHMkZ6UE59Gt+EumiJ/SKQPXTlx8h3nzrUKwzstdkXdrJy8a59M7OPhkitzoGdwv2infy9OTH+lRHavhk84iESWsioxZ47lWZHOMLjSDggF9yrcwcbVKyyfahfLf3jTj+Jx8vOkyru7ASMmnGAAvI8juD6/KgjuyvB44TOy2kdo0jIGSMJoOlBupUDK6mcAkKeeQKdWs7ovdhCzA9dlHLJJ5888h5cgc06tJ21Mj4JX7w28tiPPBHv8hTugZ2t0xco4GR1GR0B5ZPQ889D5U7JqNu/DOp81H91gv8JD8qkKCp/SnwY3fDpYVdEctGU1sFVn1jShJ2yxOB+sVr3sp2oD91Z3SNb3wiBaJl2YL4WdGXK6SQds55jfGasPErCKeMxTxrJGcZRxlTg5GR7wDTfh/BLWDeC3hiPnHEiH5gA0GbdtOJx8Vf6vmRbW2lPeqkUslxcSJkaI1RSFTdhrY+0eXhydK4VcGSCFzG0JaNGMTZDRkqCUOQDleXIcqeFq4oEjAurVpGrzwM/OvWNNp+KQqdJcFvwrl3/srk/lSLcQJ9mGVh5kKv5Mwb8qCO4v9hcQ3A2WQiGb1DH7NverVOSjFV/ixa5aK306CXV21EZ0IctgdTnAqUub2QsdMLc+rAfwzQLGoDiELW3ezwsNBOuWJzhWPVlP3X+YNSJa4PSKP1JaQ/LC/wAa4Th66g8pMzqcrqwI1PmqDbPq2SPOgcA6o43wR3ihgrDDDIzuPOo+6syZY5kco6BgNgQQ2xBFSErknJOTSD0DCSxU+0zt73YD5ZxRFEsf6NVTPMqACfjzo/0hEZDGrhnHtKviK88asZ05wcasZrpzQR91OI57aZvZSXDHyVwVJPzrzin2ZYEHIOABuSScKB5k7UpdxK6lWGQRgim8GU0ksXdBhHbmg5bdNWNtR3FA37ptUkb4Eirqwu6kD21B6svX41U+IuyyspCHvGTScktpUAqpGPCusOck9TgEna2W3/iIT5d6T+z3T6v5VBXdqhztgllckbEspBGT8APdtQRloSj+qvscYznDEj4sR8K+hgc18/TLknyAJPw/qcD41vtr7C/sj+FA0vEJcaTggEjqOmxHUf56V5bRvqLvpGRjC5Puyfn8/n3xCTQVYgkYI2BY7kY2H+d67jcEAjcEZFAzjkMRbUjEE51qNXwIHiznJ2B59KVPFYOsqD0LAH5HenWa8wPKgiZZjLIGRSVRcLkFNZLKWIyM6QFG+MeLbNOddwekSfFpP/wpLhhy0h/Wf/GV/goqQJoGRFwPvxH07th/9yjh973gOw2LDIO2VOGHoQff7654zJIsLmL29gDjOnJALY64GTj0pCynghRYwSAu2WDb75zqxgknJJ8zQK8bvWhheRF1Mo2BzjcgZON8DmfdTeKwRx9rI056gnTHv5IuAV8tWr30q/FozsgeU+SIxH9ogIPiaQ4XZOrOzqI0OO7izqZT94kjYA/hBI65oH8USoNKKqr5KAB8hQxr0muCaCFtjnibZ+5bbfGQZqXY1CcSJgukucExshilIGSozlX9wPOns/EVGNKvIzeysaltXrn2QPUkCgdMaRlbAJAJx0GMn0GSB8zTQ2UrAmeQox9mKJsKvlrcYZm9FIHv50hwO7LmSGQYmixnBJDKfZdckn3jNBCR9rHe/NiITC4TvC8pDZGAcKqNjOCd9W2DsaedqL8qghjRZZ58rFG+6frO4/4aDc+ew5mqZ9I9yLPithekEgq6MBgZ05B3OBnTL1PSrb2esj4rqUq88wGSp1JGg3SKM/hHMke02T5YCm9ibU2XFru01FlkjWQMQBqI0nIAGAPHJsOWK0KQ1B3fZkSXq3jSMjomhVjwAR4sliQckhuQAxgbnnUy1AjIaZJGJJXV90jiMhGSAzFgqA430jJOOuBTqQ01sG+0uh17hMe4Sb0DMYQMEAGoYOOePLPly2qOuTT5gWJC4wPaY+yvlnzJ6KNz7t6aKFDOwyQkbkluZ1DQu3IeJlOB5czQRlwPszjmzkN8ACg+JLf2R5VvyLgAeQxWG8GjV7iKHILGSMyL1XJAQHyIDFvj6VulA1v30rqwTgjlz32qCuOK9yQNURVmYAO5jY75JVsFSPEowdI5eLcCrHcR6lYeY/PpUR9WVgCvgO5BAB9oeIEHYqeo9ByIBAQNlxeWa6Ze9ng7xS1vE8EbxYQBZu8KgtqWTn9qgw6Ab5q1W7uV8YUNvnScjnsRkA7jfHTlvzqr8N4a1s9uulmWOa4j1kD9HOvfZ25L3wjjA9BVpzQR7kxOWwShJJwCSM7nYbnDZ2HRvQ0LfSyH7KHC/jmJjB/ZXSWP7wX409nnVFLOwVRuSTgD40hDxKJyAHGTyBypPuDAE/Cg5s73XswCsM8jkbc+YByPIjqOdI8R4xBDJDHK2l5ywj8LEEqAWyQCFwDnxEcj5UhE3+tTAcgIif2iHB+agfIVUfpT4fmJmW3t0EssEc9yyI0xRmVMqQuQBsupmzjIA5GgtXaPtEtosDFGl7+ZIU0FQNTg6CSSBpOOdRvaTj95bW8k/wBUi0RrqIa5OsjIzhVhIJ/e6VBdreE3CcFnEojEkDLLFHGcxRpE66VU6FbAjDHxZOSdxnAcydm/rML6bDh8ffRHTJraWUB18JB+rqQcHOdXlQXWCcOiuvJlDD3EZFek1WPo34iJOG2mTh1j7vSThsxEodufJc1ZCaDxjTae4jiXLMsa+pCgn+ZpcmoXs3iR7m5cBmWQxRE76FUb6fIkkZNA6/0nCf8AeLj1OP4007P+O5urkbxBFiVujtkFtJ64xipJ41znAz7qJJCdidhy8hQMnso9feFFMg5ORqYDqFJ3UcthgUnPeqG0DLv+BAXb3kDkPU4FNeMXRV4lYtHCxPeSLzHkufu5P3ulPlZVTEShIzuAnJvUtzY+pJoGUksx5RBf23APyXV/GubZ9cCTdHLADGPZOCfdnrSPGw5iYR+0dttjgnxY9cUpeSM2kJoijRQqKcuwAHUDAz8TQJSNUZIzAmdDhVVlZtsSDqi55nO+eS9d9i7eJfvZkP6+An9hcA/vZpteTZyWOwGPQDyA5AelB7xfA0hNoioaMejDOT5sepO5qq8VvyjaFcIWxqY4wozhSAdi5OQufU/dqwXxKxW6H2lhXPpqLMo+ClaqfHLMyadJwdS+WMBlbJ2ySAMAetBYfonsA3E2KDCRoS2xyzDbJYnc5cc98ityqhfRLwzRDJNjGshF/ZTmfixI/dq+0BUHxWFcsjhdDbgsAVBz1B29rB+OPKpym1/DqXbmPzH3h8v5UEKCqRmSM92FBLL7Ue3MY6ehXHQ4NP4ZdSgkYPUeR5GmsXdkFV04BAIXGxwCAQORxpOPLFAZ02x3i9CuA494JwfeDk/hoOeMDKxjp3sefg2R+YFL92roA4DAgc96ZX99EVw0ixnII7z7MgqQV2bBxkVxacUXSPDIw+6UjeQEe9FIyOXwzyNA/traOMEIoXUck7kk4xkk+m1NOO8Kiu4JLeYExyABsHB2IYYPnkClPrueUU//AMph/ECuH4gB7STD/wBJyPyBoPOK8NS4jMUpcowIYK7JqBBBDFSCQQTkUxtuy9rGoQI5RQAqvNNIoA2AAeQgAADYbVJQXaP7DA45+Y945j417LKFBZiAAMkk4AHrQMrHglrAcw28MR80iRT8wM08JplHftIA0MUjqeTnEaH1Bcgkeqg16Y5z7TxR+iK0p+Z0D8jQOSaheyT4gmjPtJPJqHvwVPxwafNZA+1LM37yxj+4oP50xuuHiNZHtx9qw31s76wOhy3PyPSglGNNLm6RN3dV/aIH8abWUcE0ayKhIOzK7yMVYe0p1Md/5UvFEkf6NET1VFB+eM0CDTPIPskyD9+TKRD4kZb3KD8KLSFIIe5jJbLF2YjAJPRV+6npSsshO5JPvqmXvbdRNLBDa3E7xnTlFwhYe1kn2QDjfG+/TGQtEjU2kaqw8PE7n9JJHZRn7sf2k2OoLZwD6r8qYcL4UtrxLSjOwktSzM7FmZxKBqPwwPnQWvd20KQMKXZjyVR1IG5JOAB1J6VHkgbv4x+E4Cny1Y3I9M04srgB7lD7UixlPUKclfiTn4Ux4lhUIyC/MkHKpjfC9CfNvgNslgRv7gszMxyxJJPrUfBC0kiooyzsFA9ScCpLj28znkPCT+0VBIHmck7VaPo34GGlM7DaLKr6yH2t+uldvLJyKC/8JsVghjiXkige89T8Tk/GndFFAUUUUFY7TWhi1TxoxOkhhGdLuuDlVP4xuyZ65GQGJqrdluP3Yh1Tg3Kxu0cpWPu7mMqdmaLOJFKFX8GGwwwrVpssYYEHkarNxbCGbJ8JK6c4wrKp8G/IFckD9o89sA9tL0OivG4ZGAKspypB5EU24mxGJNR8PMnkB0b3DJz6E+lVeyt9N9dKELQKElAIOhJZf0wXpk6FfbkZWO2asFzal42WOXCupHiBkwCMZB1Bvnq+FBIRy5GeXmPI9R86TupyiMwBYqCdI5nA5D1oRQowDn189sUO222xoIzhMqqg72VTM5MhBYDSGwdC5+6Nh780vxCSFo2WVkCMCDlgBj35qJl4jdRyukls91FpQo8aQjxeLvAQ842A0Y2/F6GvE4vKu8fDJlPn/qaH599mge8Omd3CxFpI1X9KQe7wNgpY4BOOqfEdakGNV++41flCVsGY/ha6jVufmurpvsaOEX7rCUmk13Cq7lSrIcZJUKGAZ1XIXXjfG+5oJ1mpMtUXwm2je3jkkVZZZRrZ3AfGSfCudlA5YFdy21ugy0cSjzIAH8hQJ9nV3vXH6IyKF8i4B1kflTp2rhb1WAVCulR4VUAKB6ADFJu9AO1ISPQ702DltWjGFOGYnCKfInBJb9VQT6Cg8keo2aBO9EuPtAhQHJ9nOSMZxzxTqfT1LP8A3F+AB1fNvgKa20wWNnQANKCv4QiA/NnYjmAcDA6mgRmkpsEyC7fo1GT+tuAFHvO2egDeWKJplH6x+S/LmfjgeYpETd4qoMs7Pkrj2jgLENvugathv4sD0D20tZr2dVX2pGO+PCo5u3uGcnqSR1NbbwuwSCJIoxhUGB5nzJ9Sck++onsh2eFrHqfeZwNR28I5hBjbGdzjr6AYsFAUUUUBRRRQFIXlqJFwdvIjmD50vRQVK7ikiIXGTkDbZSCcFgOmPIfI1yIUbfxAHfCyOgPr4T/CrVc26uulhkfmPUVWuIWDRHO5X8Q/+ocs+v50CDyFDzJQ7YJ1Ee4nc/EnPLnjLlZARkcjTJnJGPAwPPVlfhsGpDhrt3YyyMfFndhuGZeqc9hnlvk9aA47w95gmiVoyjaiup1RxgjS/durY6jBxnGQeVRlpY3ya1SSJFYggu810U2wQgfQQCd8M7AEnG21TZkb8OfcyfzIpOS60+0rL06N/gJoGL216uyXUTbDeW3Jb1OY5UH92kIeDyNPHPcz940Wru0jj7qNSy6WOCzMxxtu2PSpbvM03W7DFggLlThsYAB8tTELn0zmg8htAmoKzKrEnSMeEnmVyDjPluPSu4lRDqVcv/xHOtx+ycAJ+6BXDFv1F97Fj8lUr/epJ/Vyf2UC/wAWf+FAz4hcGGTv8koxAmUnORyDj9YU+vU0MVqPijWSco5Zo4k71gxXcgjQuyjbO5zXVzMGJZhknclmY/lq0/lQEkwzjqeQG5PuHM02toBE2piRHkkwk5LE+n+79fvbez1oFyTlU2BGSEXAI8/CMEetMpZqDqebnTCeavJpvLf3VzY8MnuJe6iQs3XyX1J5AUDaNGkYKoJz5DJ9wHUnkB51qXYzsiLf7aZR3xHhXmIh5Z6uerfAbc3fZTsnHZrk+OU+0/Qc9lHQbnfmfyqx0BRRRQFFFFAUUUUBRRRQFeEV7RQQnEOAg7xHSfwn2T/T/O1Vu6t3ibxKUJPllSffyOwHI9Kv9cyIGBDAEHmCMigoH1g5xmMe9m/khpvKytIhfmh1BTuhPRsdcHBGetW287LwsSyZiYjHh3BHkQenoMVDT9mJ0YMndyac6funcYOx25HzoGTykkkuMn/y/wD+lNpYlPNic4zpVUJx56i4Pyr24sZ09qJ/grMPmARUfJdYznbHPO1BIyzDOw0joMk/mabvNTA3eTikpboDmwHpvn+FAvJIEYyBtOV0v5MPX+or2aQpjKgNsRr8TDqPDyU+9QwpD6nM5XRDM+GDY7s4ODkZ9KlI+yl5NlmQIzMSTI/hwfRcsD6b/CgiPrgV+9JLS4IU74BYYLEndjjYDGBz3pmCWwArPKTgRgEn5Lv88c+tXzh/0eoN55mf9WMd2vuJ3Yj4irVw3hUNuumGNUHXA3PvPM/E0FB4B2Fnc67lu6X/AIa4Lnz5eFc+mTgkbVoPDuHRQIEhQIvpzPqTzJ9TTqigKKKKAooooCiiighu0kl8iarJYJCoJMcutWY9ArA4B94xnqKoXYf6ROIcSuWgSC2h7tdUhcyFgNWkgLsdWdt8Y69AdWrDe1w/0Px+K8Xa3uiWkxywxC3I9cEpL6k0Ft+kLtjxHhmJO4t5rd2Kq4Mish3Kq4yRkr94bEg7DbMzwTiXELixS5X6mJJUSSNftSgVlJ0u2rOrdRkDAw2xqW7UcFS9tJrdiMSphW56W5xsPcwU/CsU7DccujbycDAZJ3maMP8A8GIlvrnxUhtPmZPQUGh/R92o4jxGNbhobeC3LYyTIzyAHD6BkAAHI1HqORpG+7UcVTiK8PSC0kd4++EgMqqseSpZxuRhhjAJzlfPa92NpHbwpHGAkcSBVHQKowN/cOdVb6PkNw1zxJwc3b4hByCtvHlYdjy1HU589QoLZaB9C96VL48RQELn0BJOPeaWqice7XXttfW1mILeQ3We7k7x1Ax7WpdJ5DfY7+lWbis1zHAXjELyIrMysWVWwM4UjJHxBoJWiqj9Hnaa44jALl4ooomLKqhmdzpOCScAAZz50ovG7yS8lgt44JIYtnnLOAj5P2WAp1yAaScEAZ3wcAhaq8IqF7V8RuLa2kuIVifuY3kdXLLqCjU2kgHBwGwCPLcc6Zdge0Fxf2y3UkcUUcmsIqszv4XKEsSABurbDO2N+lBYzbJ+BfkK7SMDkAPcKod92wvY+JxcO7i3Z5k7xZe8kChMSE5XQTq+zcYBP3dxvh1xztlLYTQrfQp9XnbQLiJyQjeUiMowMZOoMdgdqC6UVSPpI7YXHDI0nEMU0TyCMAuyOGKMw+6QR4W326fCYnm4mI8rDaM+M6O+lAJ6gN3XyJA+FBP0VAW/FriWyS4ijjEugmSKRmADrkSR6gDgq4ZdweVRHYftRecSthcpFbxKWZQGd3J08ycKMb++gu1FVC/7YvZSxpxGFYopW0pcxOXh1b4WQFVaM4HPxDnvgEizX5l0Zg7sv07zVpO3mu43xvg+6gc0VSPo+7XXXERK7QwwxwyGJsOzuWABbHhAA3G+/WrvQV3t/wBo/qFjNcDBcALEDyMjHC5GRkDdiPJTT7svxhby0guVx9qgYgdG5Ov7rBh8Kq/afhq8UvJbRv0VrAxY9BczqVhOPOOPU49ZF9Kr/wD7P/F27q5sZdpIHLqp5gMdMq/uyAk/t0GuUUUUBVJ+l/s79c4dJpXMsH20fUnSDrX11JqGPPTV2ooM++h/tStxwwd64DWg0SMx+4q5jc+mjYk9Uas37SXFzb3dvx1U0x3MxZEAwe7ChUD+s0IZvTepGz7FTpxm54fEWSynVZZcZANvr1KgIOQdYeH9nXWt9sezqXtlLa4C6l+zONkdd4z7gQPhkUET2z4oLi1t7e2fLcSKxow5iErrnkx6RZHvYVbbS2SKNI41CoihVUcgqjCge4AVln0G8In0NPdah3Gu2t0b7g16rn5yYXPTQRyrWaDM+3P+3uDf+t/hrQuKfoZf2H/wmsw7acUjbjXDJU1vFB3glkSN2RNWQMsFI9+OVaBx3jMKWryFwVdH0aQWLnSdlABJPuoMe7D9rfq3DLK2kEtvDPLKsl4ANKDUx0oc5VzyLkeEZIBxldx4baRRRJHCqrGo8IXljnnPXPPPUnNZn9FFlBPwgWF2hDFpNUUisjYLFlZcgbjIIZeRFNOx/aGXhly9hKZbqyU/Y3CRu/dA76G0qcqORxnSeWxwoaJ27/2bf/8AS3H/AGnqE+hb/Y1p75/+/LT3txxeE8NuQr6zPbzpEEBdnYoyAAKCfaIB8utRn0M3SrwyC3bKTR98XjdWRgDM7A4YDIwy7jzoIXtTdLF2osXfOkWpzhWY7/WwNlBP5Uv9ItvJxg29laxS90soknuHjeONAFZcKXUa2wzHC530+uGXGeJxt2jtLldbW8cBjeURuYwxFxjxBcY8abjYZ9DjWopQyhlIKsAQRyIPIigy/wD9ogf/AA2D/qk/7M9WXtHxy7QQBbWSJXubWN5TJEQiNPGG2VmJ1A6P3qq30+ziW0it4Q0sy3CuyRozlVEUgycAgbuux86u952osu71SMWUaW09zIzZBDIdOjOoMARtkEDyoJm8H2b/ALLfwNUH6A/9kR/8yX/FU1Y9p+8tJbqdWhjkMggjZG74oo0jUoyS7MGYADkVG/M1n6GOJR23DViuC0MiySErIjocE5BGRuPdQWv6TLNJeFXquAQsLuPRkGtD/aUU2+iS9eXhNoznJCumfMRyPGv91RUX21vrjiUZseHxvolIE91IjRwpHnJVCwBkY4AOnIwfXItdhBb8OtIoi2mKJQgJ5sQNzgDdmOTgDzoKV9BP/h77/rZf8KVofFL9IIZJpDhIkZ2PooJPx25Vmn0IXaxx3UUoeKSW5eRFkRkLKyrjGoAE+E5A3Fc/Sl2rSdI7OBJ5Inlj+tSJDLhYlYF1U6RqY88jbAI60En2I4dxP6ubhZbVGvHNy6ywSyOveAaF1CdRhUCADSMVSbnveFdoYpZ2j03RzI0SNHGRKSj7MzEaZAsh8R/PFbdwriEU8YeE5TkPCy4x0wwBHTpWTfT13VykCQrJLcxSMrKkcjARsvjyQunOpY8b55+tBstFUnsB21W5hhiuFliugoVlkikUOQPbViunxAZwSCDkepu1AVHcbvpYkXuIhNK7qioW0L1Lsz6W0qqBjnByQANyKkaKContRc/X1sRawGUwd+zC5fSqatAB/wBWySWx86Q4n2uu4UCvaRrM92lqiiZnQ640dZQe6UsgDNkYXGk7+SXYk9/xLi13nKrJHax+ncr9qB6FyDSfHllu+MwQwnSlnC0skmx7uSbKJhSCpk0ByuoEDUWIONJC/AUVmXC+0NzFDJGZJJGn4jNb2sjDvJFhQ/ayAKp16dEunIPi05GnNXLspbTpHKZ2c65WaJZGDvHHhQqsw2JJDNjJxqxnagZ9rO0s1pNaxRW8cxuZO7XMxjKnGSxHdN4ANyQc+hqQhv7kTpHLbxrG6ue8SZnwy4wpUwpzBJBBPsn0zV+LI9zx6BEKgWVq8uplLgSTN3ekgMu+gZG9M+2dxfw2yIszC7urwxxiIBcjXhDk69EYgQEgZOXyTswIaXRVG4lcT2s8UJuZ5pb5icBFIgSJS0xhVV21ExourOM6iTgkox8Suofq9nIZWmmM8zBcSzR26sTFDr9nvCWjQyE4GHw3stQX+qtxXtNcR38NlFbRSGZGkDm4ZNCr7Rde5bGTsME5PlzqU7LW08drEt05eYaixLaiMsxVC2BqKqVXV10561U+ERyXPG+ITowUW0cNqjFdYOftZRjUMEPgfGgtvDb64aaWOeBIwixsjpKZFfUXDDeNCpXSPP2hUpWfdrb+5tOEyEzsbvOD3YHeGWVz3aIMnSBqGwydK4BzvSUPFLiK+zPcSGK0smmvEGgxqSPsU2Goy6Vdy2Tk8sA6aDRqKqXDbS6uYILr6zJDLKmoxqV7pElA0jSyHMkSnIb7zg58J0iE7OccnKCKWdybO8u1nlbGp7e3QnLYGCSZLcHzGaDSKr1tx+STiUtmsS93BEskkuvUdT/o49OkaSQGbmdh60z4JM80EXEZ7iSJSrz90CohWAqTGjgjchNLl851FsELgCtcIv50topI/DecZuS6swDdzCVyrAcm0W6qVB6tvkbUGpUjbXSSAmN1cBipKsGAZThlOORB2I6VQLvtLJarxjTITFZJD3bykyZmZCXTUT1YxDHJS3LHhpjwiOThcvD4e9cp9VuZ79WYuq6VVzIudwe9ZhnrQanTXil53MMspxiNGc6m0jCgk5ODgYHPFUnhXGZ3WPiUzyR2q28k0ynAjIYBoI40I1MyKCTIcai3hyDhGXaaSW5itbeaR1ueJOo7lHIWC29ucEDZ2EQILNnLE6dIGAFn4Vx+6ltbOf6pqa5ddaLIB3ML5KyEsBrITQSABzPlvZqpAv5P9JSfbmOzsISZlGBEGZcxodskpGDITnbKAAbksu0PFbm4FrBHNJbS3zgpFHhZIrYeKWWRipZZCg2ClQGYKNRUsQ0SivAK9oCkrqIspVXaMn7y6Sw92pWX03BpWighOzXZqOyDrFJKyOzOVkKt429t86Q2T6kj0riTsumqR0nnieWdZpGjZQzaUEaxHKH7IKBtzzvmp6iggOL9k4Zo7dEaS3NswaF4SFZPCVIGpWBBU75BqXtLNY4xGhYAZ3J1MScksSc5YkkknmacUUEBwvsqkFzLdLNO0s2nvdZjIk0jCZAjGnSOWjT65p7dcFikuYbp8mSBZFjG2le8062xjOrC6c55E7VJUUEH2g7Mx3UkExlmhlgL6HhYK2lwA6nKkEEAdMjGxFI8R7HwySQSpJPA8KNGGikwzxsQWR2YFjkjOoENkk5zvViooEDbAR93GTGAoVSunKgbDGoEbDzBqD4R2RW273uLi4TvpGlkOYmLSN7TeKI4J8hgelWOigrsPY6AGIs8spjuDc5kYMXlKaFZ/DvoXAUDGMDoMV5/7m25e7aQySfW9XeBmGFDR92dGAMeDYE5IBOCMnNjooK5w+K14akcc10xZl0xm4kXvGWMDCIAFB0gj2VycjOTTbst2YVYrtrhTqv5JZJIz92OTIWM+TaCNWPvE7kAVJcd/T2f/MP+E1NUFWg7ERLZyWZnuHR4hCHd1LpGNgiYUIBjmdOTtkkAYdnsrCFg0M6SQMzJKCrSamjMbltSlTlDjBGBpUAAKBU9RQZn2isLbvrLhMEmnvZ3ubltatKxQFwZC2SXklKnl9zlgYq6Ds5CRP3paZrhDHK7kajHggRrpACoNTbADcknJ3r28/SL/wAxf4ipegqkPYSAWb2jy3EqPGIQ0jgvHGMaVTChVxhTnTk4XOQAB6vYS3M8U7yTSSRoUYu4PeqcbSYUeHYeFdKncEEM2bVRQVU9hLcxXcReZvrbSs7Fl1J3hBcJhcAeFeYJwoBJAAHVr2Gt0uUuTJNJIsIiPeOGEgDB9T+EEtkLsCFwANNWiigKKKKD/9k=)

Texten inom hakparanteserna är en bildtext och bildens address anges inom
paranteser.

Gör en länk till en valfri bild på en elefant istället för texten på denna rad.
![elefant](http://upload.wikimedia.org/wikipedia/commons/2/22/Afrikanischer_Elefant,_Miami.jpg)
